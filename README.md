   **Microsoft Activation Scripts (MAS):**

   A collection of scripts for activating Microsoft products using HWID / KMS38 / Online KMS activation methods 
   with a focus on open-source code, less antivirus detection and user-friendliness.

   **Homepages:**<br/>
   NsaneForums: (Login Required) https://www.nsaneforums.com/topic/316668-microsoft-activation-scripts/<br/>
   GitHub: https://github.com/massgravel/Microsoft-Activation-Scripts<br/>
   GitLab: https://gitlab.com/massgrave/microsoft-activation-scripts<br/>
   
  # **Downloads:** <br/>
  
  Latest Version: 1.3<br/>
  Release date: 22-jan-2020

   https://gitlab.com/massgrave/microsoft-activation-scripts/-/releases

<br/> 

<pre class="ipsCode prettyprint lang-html prettyprinted"><span class="pln">----------------------------------------------------------------------------------------------
Activation Type       Supported Product             Activation Period
----------------------------------------------------------------------------------------------

Digital License    -  Windows 10                 -  Permanent
KMS38              -  Windows 10 / Server        -  Until the year 2038
Online KMS         -  Windows / Server / Office  -  For 180 Days, renewal task needs to be 
                                                    created for lifetime auto activation.

----------------------------------------------------------------------------------------------</span></pre>

  # **ReadMe:**
   
<p>
<details>
<summary>Click me to collapse/fold.</summary>
<br/> 
[Digital License (HWID) Activation](https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.3/Separate-Files-Version/Activators/HWID-KMS38_Activation/ReadMe_HWID.txt)
<br/> 
[KMS38 Activation](https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.3/Separate-Files-Version/Activators/HWID-KMS38_Activation/ReadMe_KMS38.txt)
<br/>
[KMS38_Protection](https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.3/Separate-Files-Version/Extras/KMS38_Protection/ReadMe.txt)
<br/>
[Online KMS Activation](https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.3/Separate-Files-Version/Activators/Online_KMS_Activation/_ReadMe.txt)
<br/>
[Activation Methods info and faqs](https://pastebin.com/raw/7Xyaf15Z)
<br/>
[$OEM$ Folders (Windows Pre-Activation)](https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.3/Separate-Files-Version/Extras/Extract_OEM_Folder/ReadMe.txt)
<br/>
[Big Blocks of text in the script](https://pastebin.com/raw/DdM34pr5)
<br/>
[Download Genuine Installation Media](https://pastebin.com/raw/jduBSazJ)
<br/>
</details>
</p>  

  # [**Credits:**](https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.3/Separate-Files-Version/Credits.txt)

  # [**Changelog:**](https://pastebin.com/raw/nghFEt3W)

-------------------------------------------------------------------------------------------------------------------------------------------------

HWID/KMS38 activation's various methods:

by @mspaintmsi and *Anonymous, Original co-authors of HWID/KMS38 Activation.

https://gitlab.com/massgrave/massgrave<br/>
https://github.com/massgravel/MASSGRAVE<br/>
https://discord.gg/EcENUnq<br/>
https://www.nsaneforums.com/topic/316668--/?do=findComment&comment=1497887

-------------------------------------------------------------------------------------------------------------------------------------------------

For any queries feel free to mail me at, windowsaddict@protonmail.com



Made with Love ❤️
